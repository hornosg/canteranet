<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use DB;
use App\Models\Comprobante;
use App\Models\ComprobanteItems;
use App\Models\ComprobanteMp;
use App\Models\Cliente;
use App\Models\Titular;

use App\Http\Controllers\ClientesController as ClientesController;
use phpDocumentor\Reflection\Types\Integer;

class ComprobantesController extends Controller
{
    /**
     * Adminstracion de Comprobantes
     */

    private $dir = '';
    private $accion = '';

    /**
     * Inicializa variables
     */
    private function _initVars($tipocpte){
        switch ($tipocpte) {
            case 1:
                $this->dir='presupuestos';
                break;
            case 2:
                $this->dir='tickets';
                break;
            case 3:
                $this->dir='presupuestos';
                break;
            case 4:
                $this->dir='presupuestos';
                break;
            case 5:
                $this->dir='presupuestos';
                break;
        }
    }

    /**
     * Muestra un Formulario para crear un nuevo comprobante
     *
     * @return \Illuminate\View\View
     */
    public function new()
    {
        $tipocpte = 'tickets';

        $view = 'comprobantes.'.$tipocpte.'.form';
        $accion = 'Nuevo';
        return view($view,[
                'accion' => $accion,
                'edit'=> 1
        ]);
    }

    /**
     * Inicializa el Comprobante
     * @param Request $request
     * @return Comprobante
     */
    private function _instanceComprobante(Integer $id=null)
    {
        if (empty($id)){
            $cpte = new Comprobante();
            $this->accion='Creado';
        }else{
            $cpte = Comprobante::find($id);
            $cpte->updated_us = auth()->id();
            $this->accion='Modifiado';
        }
        return $cpte;
    }


    public function save(Request $request)
    {
        //dd($request->request);
        $this->_initVars($request->id_tipocpte);

        $items=json_decode($request->items);
        DB::beginTransaction();
            $cpte = $this->_instanceComprobante($request->id);
            $cpte->id_tipocpte = $request->id_tipocpte;
            $cpte->numero = $request->numero;

            $titular=Titular::find($request->id_titular);
            if (empty($titular)){
                $titular = (New ClientesController)->newclienteByDescripcion($request->id_titular);
            }
            $cpte->id_titular = $titular->id;
            $cpte->serie = $this->_getSerie($cpte->id_tipocpte,$titular);

            $cpte->observaciones = $request->patente;
            $cpte->id_estado = 2;
            $cpte->created_us = auth()->id();
            $cpte->updated_us = auth()->id();
            $cpte->save();

            //Elimino los detalles de comprobantes para volver a grabarlos
            if (!empty($request->id)){
                ComprobanteItems::where('id_cpte', $request->id)->delete();
                ComprobanteMp::where('id_cpte', $request->id)->delete();
            }

            //Grabo el mineral
            $cpteitem = $this->_saveComprobanteItem($cpte->id, $request->id_mineral, $request->cantidad);
            $cpte->total = $cpte->total + $cpteitem->importe;

            //Grabo los servicios
            if (!empty($request->servicios)){
                foreach($request->servicios as $item ){
                    $cantidad = 1;
                    if (($item==4) && (!empty($request->kms))){
                        $cantidad = $request->kms;
                    }
                    $cpteitem = $this->_saveComprobanteItem($cpte->id, $item, $cantidad);
                    $cpte->total = $cpte->total + $cpteitem->importe;
                }
            }

            if (!empty($request->id_mediopago)){
                $cpte->id_estado = 3;   //Si el MP forma parte de request es un pago o una cobranza, queda como registrado.
                $mp = new ComprobanteMp();
                $mp->id_cpte = $cpte->id;
                $mp->id_mediopago = $request->id_mediopago;
                $mp->numero_transaccion = $request->numero_transaccion;
                $mp->cuotas = $request->cuotas;
                $mp->id_banco = $request->id_banco;
                $mp->fecha = $request->fecha;
                $mp->importe = $cpte->total;
                $this->_saveComprobanteMp($cpte->id, $mp);
            }else{
                $cpte->id_estado = 2;
                $mp = new ComprobanteMp();
                $mp->id_cpte = $cpte->id;
                $mp->id_mediopago = 2;
                $mp->importe = $cpte->total;
                $this->_saveComprobanteMP($cpte->id,$mp);
            }
            //Actualizo Total y Estado
            $cpte->save();

            if (!empty($request->id_flete)){
                if (empty($request->kms)){
                    $request->kms = 0;
                }
                $idestado = 2;
                $lastpago = $this->_savePago($request->id_flete, 4, $request->kms, $idestado);

                if ($request->gasoil>0){
                    $idestado = 3;
                    $this->_savePago($lastpago->id_titular, 6, 1, $idestado);
                }
                if ($request->adelanto>0){
                    $idestado = 3;
                    $this->_savePago($lastpago->id_titular, 7, 1, $idestado);
                }
            }
        DB::commit();

        $data['msg']='Tickets '.$cpte->id.' Generado Exitosamente!';
        return $data;
    }

    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        return view('profile.edit');
    }

    public function listCptes(Request $request){
        $tipocpte = 'tickets';
        $view = 'comprobantes.'.$tipocpte.'.list';

        $cptes = $this->_getCptes($request->idcliente, $request->desde, $request->hasta);

        $total = DB::select('
            SELECT 	FORMAT(SUM(c.total),0, "es_AR") as importe
            FROM comprobantes c
            JOIN titulares t on t.id=c.id_titular
        ');

        $minerales = $this->_getMinerales();

        $servicios = $this->_getServicios();

        if(empty($request->ajax)){
            return view($view,[
                'cptes'=> $cptes,
                'total'=> $total[0]->importe,
                'minerales'=> $minerales,
                'servicios'=> $servicios
            ]);
        }else{
            return array(
                'cptes'=> $cptes,
                'total'=> $total[0]->importe,
                'minerales'=> $minerales,
                'servicios'=> $servicios
            );
        }
    }
    private function _getMinerales(){
        return DB::select('
            SELECT 	FORMAT(SUM(c.total),0, "es_AR") as importe,
                    ci.id_item,
                    i.descripcion as mineral                    
            FROM comprobantes c
            JOIN titulares t on t.id=c.id_titular
            JOIN comprobantes_items ci on ci.id_cpte = c.id
            JOIN items i on ci.id_item = i.id and i.id_tipoitem = 1
            GROUP BY id_item,mineral
        ');
    }

    private function _getServicios(){
        return DB::select('
            SELECT 	FORMAT(SUM(c.total),0, "es_AR") as importe,
                    ci.id_item,
                    i.descripcion as mineral                    
            FROM comprobantes c
            JOIN titulares t on t.id=c.id_titular
            JOIN comprobantes_items ci on ci.id_cpte = c.id
            JOIN items i on ci.id_item = i.id and i.id_tipoitem = 2
            GROUP BY id_item,mineral
        ');
    }

    private function _getSerie($tipocpte,$titular){
        if($tipocpte!=3){
            $serie='X';
        }else{
            $empresa=Titular::find(1);
            if ($empresa->id_tipoiva!=1){
                $serie='C';
            }else{
                if ($titular->id_tipoiva==1){
                    $serie='A';
                }else{
                    $serie='B';
                }
            }

        }
        return $serie;
    }

    private function _saveComprobanteItem($idcpte, $iditem, $cantidad):ComprobanteItems{
        $cpteitem = new ComprobanteItems();
        $cpteitem->created_us = auth()->id();
        $cpteitem->updated_us = auth()->id();
        $cpteitem->id_cpte = $idcpte;
        $cpteitem->id_item = $iditem;
        $cpteitem->preciouni = (Item::find($iditem))->precio_ref;
        $cpteitem->cantidad = $cantidad;
        $cpteitem->importe = $cpteitem->preciouni * $cpteitem->cantidad;
        $cpteitem->save();

        return $cpteitem;
    }

    private function _saveComprobanteMP($idcpte,$mp){
        $cptemp = new ComprobanteMp();
        $cptemp->created_us = auth()->id();
        $cptemp->updated_us = auth()->id();
        $cptemp->id_cpte = $idcpte;
        $cptemp->id_mediopago = $mp->id_mediopago;
        if (!empty($mp->numero_transaccion)) {$cptemp->numero_transaccion = $mp->numero_transaccion;}
        if (!empty($mp->cuotas)) {$cptemp->cuotas = $mp->cuotas;}
        if (!empty($mp->id_banco)) {$cptemp->id_banco = $mp->id_banco;}
        if (!empty($mp->fecha)) {$cptemp->fecha = $mp->fecha;}
        if (!empty($mp->fecha_cobro)) {
            $cptemp->fecha_cobro = $mp->fecha_cobro;
        }else{
            $cptemp->fecha_cobro = $mp->fecha;
        }
        $cptemp->importe = $mp->importe;
        $cptemp->save();

        return;
    }

    private function _savePago($idtitular, $iditem, $cantidad, $idestado, $observaciones=null){
        $cpte = $this->_instanceComprobante();
        $cpte->id_tipocpte = 5; //Pago

        $titular=Titular::find($idtitular);
        if (empty($titular)){
            $titular = (New ClientesController)->newclienteByDescripcion($idtitular);
        }
        $cpte->id_titular = $titular->id;
        $cpte->serie = $this->_getSerie(5,$cpte->id_titular);
        $cpte->observaciones = $observaciones;
        $cpte->id_estado = $idestado;
        $cpte->created_us = auth()->id();
        $cpte->updated_us = auth()->id();
        $cpte->total = 0;
        $cpte->save();

        $cpteitem = $this->_saveComprobanteItem($cpte->id, $iditem, $cantidad);
        $cpte->total = $cpteitem->importe;
        $cpte->save();

        $mp = new ComprobanteMp();
        $mp->id_cpte = $cpte->id;
        $mp->id_mediopago = 2;
        $mp->importe = $cpte->total;
        $this->_saveComprobanteMP($cpte->id,$mp);

        return $cpte;
    }

    private function _getCptes($cliente=null,$desde='', $hasta=''){
        $cptes = DB::table('comprobantes as c')
            ->join('titulares as t', 't.id','=','c.id_titular');

        if (!empty($cliente)){
            $cptes = $cptes->where('c.id_titular', $cliente);
        }

        if (!empty($desde)){
            $cptes = $cptes->whereRaw('date(c.created_at) BETWEEN "'.$desde.'" AND "'.$hasta.'"');
        }else{
            $cptes = $cptes->whereRaw('date(c.created_at) BETWEEN date(NOW()) AND date(NOW())');
        }

        $cptes = collect($cptes->select(DB::raw('GROUP_CONCAT(c.numero) as tickets'),
            'c.observaciones as patente',
            'c.id_titular',
            't.descripcion as titular',
            DB::raw('date_format(c.created_at, "%d/%m/%Y") as fecha'),
            DB::raw('FORMAT(SUM(c.total),2, "es_AR") as importe'))
            ->groupBy('patente','id_titular','titular','fecha')
            ->orderBy('titular')
            ->get());

        if (!empty($cptes)){
            foreach($cptes as $cpte){
                foreach(DB::select('select i.descripcion,
                                                        i.id_tipoitem,
                                                        concat(SUM(ci.cantidad)," ",coalesce(um.descripcion,"")) as cantidad,
                                                        FORMAT(SUM(ci.importe),2, "es_AR") as importe
                                                   from comprobantes_items ci
                                                   join comprobantes c on c.id = ci.id_cpte
                                                   join items i on i.id = ci.id_item
                                                   left join conf_unidades_medida um on um.id = i.id_unidad
                                                  where date_format(c.created_at, "%d/%m/%Y") = "'.$cpte->fecha.'"
                                                    and c.id_titular ='.$cpte->id_titular.'
                                                    and c.observaciones ="'.$cpte->patente.'"
                                                  group by i.descripcion,i.id_tipoitem,um.descripcion
                                                  order by 2,3') as $item){
                    $det = $item->descripcion;
                    $cpte->$det=$item->cantidad;
                }
            }
        }
        return $cptes;
    }

    public function changePrice(Request $request)
    {
        $precio = doubleval($request->precio);
        $items=$request->ids;
//        DB::raw('update comprobantes_items
//                           set preciouni = '.$precio.', importe = cantidad * '.$precio.'
//                         where id in ($items)');
//        $query = DB::table('comprobantes_items')
//                    ->where('id', 'in',$items)
//                    ->update(['preciouni' => $precio])
//                    ->toSql();
//        dd($query);
        if (!empty($items)){
            foreach($items as $item){

                //DB::enableQueryLog();
                DB::table('comprobantes_items')
                    ->where('id', intval($item))
                    ->update(['preciouni' => $precio, 'importe' => DB::raw( 'cantidad * preciouni')]);
                //dd(DB::getQueryLog());

                $idcpte = ComprobanteItems::find(intval($item))->select('id_cpte')->get();
                $total = DB::table('comprobantes_items')
                            ->where('id_cpte', $idcpte)
                            ->sum('importe');

                DB::table('comprobantes')
                    ->where('id', $idcpte)
                    ->update(['total' => $total]);
            }
            $titularesController = new TitularesController();
            $ctacte = $titularesController->getCtaCte($request->idtitular);
        }else{
            throw new \Exception('No ha seleccionado los tickes que desea actualizar.');
        }

        return $ctacte;
    }
}
