<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use DB;

class ItemsController extends Controller
{
    public function listItems(Request $request){
        $view = 'items.price';

        $minerales = $this->_getMinerales();
        $servicios = $this->_getServicios();

        if(empty($request->ajax)){
            return view($view,[
                'data'=> $minerales,
                'data2'=> $servicios
            ]);
        }else{
            return array(
                'data'=> $minerales,
                'data2'=> $servicios
            );
        }
    }

    private function _getMinerales(){
        return DB::select('
            SELECT 	i.id, 
                    i.descripcion,
                    i.precio_ref,
                    u.descripcion as unidad,
                    i.activo
            FROM items i 
            LEFT JOIN conf_unidades_medida u on u.id = i.id_unidad
            WHERE id_tipoitem = 1  
        ');
    }

    private function _getServicios(){
        return DB::select('
            SELECT 	i.id, 
                    i.descripcion,
                    i.precio_ref,
                    u.descripcion as unidad,
                    i.activo
            FROM items i 
            LEFT JOIN conf_unidades_medida u on u.id = i.id_unidad
            WHERE id_tipoitem = 2  
        ');
    }

    public function save(Request $request)
    {
        $fields = array_values($request->all());
        $item = Item::find($fields[1]);
        $item->precio_ref = $fields[2];
        $item->save();

        return "Precio Actualizado Correctamente!";
    }
}
