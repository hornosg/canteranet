<?php

namespace App\Http\Controllers;

use App\Models\Titular;
use Illuminate\Http\Request;
use DB;

class TitularesController extends Controller
{
    public function listTitulares(Request $request){
        $view = 'titulares.list';
        $titulares = $this->_getTitulares();

        return view($view,[
            'titulares'=> $titulares
        ]);
    }

    private function _getTitulares(){
        $titulares = DB::table('titulares as t')
            ->leftJoin('conf_localidades as l', 'l.id','=','t.id_localidad')
            ->where('id_tipotitular','=',4);

        $titularesColletion = collect($titulares->select(
                                't.id',
                                't.descripcion',
                                't.cuit',
                                't.direccion',
                                'l.descripcion as localidad',
                                't.telefono',
                                't.email'
                                )
                                ->orderBy('id','desc')
                                ->get());

        foreach ($titularesColletion as $key=>$titular){
            $items = DB::table('comprobantes as c')
                ->where('id_titular', $titular->id)
                ->leftJoin('sys_tipocomprobantes as tc', 'tc.id','=','c.id_tipocpte')
                ->leftJoin('comprobantes_items as ci', 'c.id','=','ci.id_cpte')
                ->leftJoin('items as i', 'i.id','=','ci.id_item')
                ->select(
                    DB::raw('CASE WHEN id_tipocpte = 2 THEN ci.importe ELSE 0 END as debe'),
                    DB::raw('CASE WHEN id_tipocpte in (4,5) THEN ci.importe ELSE 0 END as haber')
                )
                ->get();

            $saldo = 0;
            foreach ($items as $item){
                $saldo = $saldo + ($item->debe + ($item->haber * (-1)));
                $titular->saldo= $saldo;
            }
        }

        return $titularesColletion;
    }

    public function ctacte($id)
    {
        $titular = Titular::find($id);
        $view = 'titulares.ctacte';
        $ctacte = $this->getCtaCte($id);

        return view($view,[
            'idtitular'=> $id,
            'titular'=> $titular,
            'items'=> $ctacte['data'],
            'saldo'=> $ctacte['saldo'],
        ]);
    }

    public function getCtaCte($id)
    {
        $items = DB::table('comprobantes as c')
            ->where('id_titular', $id)
            ->leftJoin('sys_tipocomprobantes as tc', 'tc.id','=','c.id_tipocpte')
            ->leftJoin('comprobantes_items as ci', 'c.id','=','ci.id_cpte')
            ->leftJoin('items as i', 'i.id','=','ci.id_item')
            ->select('ci.id', 'c.created_at', 'i.descripcion as motivo', 'numero', 'ci.cantidad', 'ci.preciouni as precio',
                DB::raw('CASE WHEN id_tipocpte = 2 THEN ci.importe ELSE 0 END as debe'),
                DB::raw('CASE WHEN id_tipocpte in (4,5) THEN ci.importe ELSE 0 END as haber'))
            ->orderBy('id_cpte', 'desc')
            ->get();

        $saldo = 0;
        $reversed = $items->reverse();
        foreach ($reversed as $key=>$item){
            $saldo = $saldo + ($item->debe + ($item->haber * (-1)));
            $items[$key]->saldo= $saldo;
        }
        return array('data'=>$items,'saldo'=>$saldo);
    }

}
