<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AjaxDataController extends Controller
{
    public function getproductos(){
        $items = DB::table('items')
                ->where('activo', 1)->get();
        return $items;
    }

    public function getclientes(){
        $items = DB::table('titulares')
                    ->where('id_tipotitular', 2)
                    ->where('activo', 1)->get();
        return $items;
    }
}
