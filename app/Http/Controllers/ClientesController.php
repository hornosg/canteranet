<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;

class ClientesController extends Controller
{
    /**
     * Crea un cliente unicamente con la descripcion
     * @param $descripcion
     * @return Cliente
     */
    public function newclienteByDescripcion($descripcion){
        $titular= New Cliente();
        $titular->descripcion=strtoupper($descripcion);
        $titular->id_tipotitular = 4;
        $titular->save();

        return $titular;
    }
}
