<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoEstado extends Model
{
    protected $table = 'sys_tipoestados';
    protected $fillable = ['descripcion', 'activo'];
}
