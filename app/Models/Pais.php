<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = 'conf_paises';
    protected $fillable = ['descripcion', 'activo'];
}
