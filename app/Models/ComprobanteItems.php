<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComprobanteItems extends Model
{
    protected $table = 'comprobantes_items';

    public function cpte()
    {
        return $this->hasMany('App\Models\ComprobanteItems', 'id_cpte', 'id');
    }
}
