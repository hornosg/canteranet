<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    protected $table = 'items';
    protected $fillable = ['descripcion',
                          'id_tipoitem',
                          'precior_ref',
                          'id_unidad',
                          'observaciones',
                          'activo'];
}
