<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoIva extends Model
{
    protected $table = 'sys_tipoiva';
    protected $fillable = ['descripcion', 'activo'];
}
