<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
    protected $table = 'conf_localidades';

    public function sucursales()
    {
        return $this->hasMany('App\Models\Sucursal', 'id', 'id_localidad');
    }
}
