<?php

namespace App\Models;
use App\Models\Titular;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Proveedor extends Titular
{
    protected static function boot(){
        parent::boot();
        static::addGlobalScope('cliente', function (Builder $builder) {
            $builder->where('id_tipotitular', '=', 3);
        });
    }
    public function setidTipoTitularAttribute($idTipoTitular = 3){
        $this->attributes['id_tipotitular'] = $idTipoTitular;
    }
}
