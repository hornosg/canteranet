<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cantera extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'canteras';

    public function localidad()
    {
        return $this->belongsTo('App\Models\Localidad', 'id_localidad');
    }
}
