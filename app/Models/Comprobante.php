<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comprobante extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'comprobantes';

//    public function __construct(array $attributes = [])
//    {
//        $attributes['items']= $this->_items();
//        parent::__construct($attributes);
//    }

    public function items()
    {
        return $this->belongsTo('App\Models\Comprobante', 'id_cpte');
    }
}
