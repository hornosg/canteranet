<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sistema de Gestion para Canteras Mineras">
    <meta name="author" content="hornosg">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="{{ asset('css/canteranet.css') }}" rel="stylesheet">
    @yield('css')
</head>
<body>
    @include('layouts.navbar')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 sidebar">
                @include('layouts.sidebar')
            </div>
            <ol class="breadcrumb pull-right col-sm-9 col-xs-12">
                <li><a class="text-primary" href="/home">Inicio</a></li>
                <li><a class="text-primary" href="#">Nivel 2</a></li>
                <li class="active text-primary">Nivel 3</li>
            </ol>

            <div class="col-sm-9 col-sm-offset-3 col-xs-12">
                @yield('content')
            </div>
        </div>
    </div>
    @yield('js')
</body>
</html>

