<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sistema de Gestion para Canteras Mineras">
    <meta name="author" content="hornosg">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CanteraNET</title>

    <!-- Scripts -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/vendor/font-awesome/css/font-awesome.min.css">

    <!-- iziToast -->
    <script src="/js/iziToast.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/css/iziToast.min.css">

    <!-- Additional Styles -->
    <link href="{{ asset('css/canteranet.css') }}" rel="stylesheet">
    @yield('css')
</head>

<body>

<!-- Navigation -->
@include('layouts.navbar')


<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            @yield('content')
        </div>
    </div>
</div>
@yield('js')
</body>
</html>
