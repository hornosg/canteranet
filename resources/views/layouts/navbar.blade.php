<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
        <a class="navbar-brand" href="#">
            {{--<i class="material-icons md-18" style="padding-right: 5px;">perm_media</i>--}}
            <span style="font-size: x-large;">CanteraNet</span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/home">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/tickets">Tickets</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/titulares">Clientes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/precios">Precios</a>
                </li>
                {{--<li class="nav-item dropdown">--}}
                    {{--<a class="nav-link dropdown-toggle" data-toggle="dropdown">--}}
                        {{--<i class="material-icons md-24">settings</i> Configuracion--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="nav-item"><a href="#"><i class="material-icons md-18">account_box</i> Usuarios</a></li>--}}
                        {{--<li class="divider"></li>--}}
                        {{--<li class="nav-item"><a href="#"><i class="material-icons md-18">account_balance</i> Bancos</a></li>--}}
                        {{--<li class="nav-item"><a href="#"><i class="material-icons md-18">credit_card</i> Medios de Pago</a></li>--}}
                        {{--<li class="divider"></li>--}}
                        {{--<li class="nav-item"><a href="#"><i class="material-icons md-18">straighten</i> Unidades de Medida</a></li>--}}
                        {{--<li class="divider"></li>--}}
                        {{--<li class="nav-item"><a href="#"><i class="material-icons md-18">room</i> Localidades</a></li>--}}
                        {{--<li class="nav-item"><a href="#"><i class="material-icons md-18">room</i> Provincias</a></li>--}}
                        {{--<li class="nav-item"><a href="#"><i class="material-icons md-18">room</i> Paises</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}">Salir</a>
                </li>
                {{--<li class="nav-item dropdown">--}}
                    {{--<a class="nav-link dropdown-toggle" data-toggle="dropdown">--}}
                        {{--<i class="material-icons md-24">account_circle</i> Gustavo--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="nav-item">--}}
                            {{--<a href="{{ route('logout') }}">--}}
                                {{--<i class="material-icons md-18">power_settings_new</i>Salir--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
</nav>
