@extends('layouts.app')
@section('content')
    <h1 class="mt-3">Precios</h1>
    <div class="card-deck mb-3 text-center">
    @foreach($data as $item)
        <div class="card mb-3 box-shadow">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">{{$item->descripcion}} $ / {{$item->unidad}}</h4>
            </div>
            <div class="card-body">
                <form id="form{{$item->id}}" name="form{{$item->id}}" role="form" class="form-inline">
                    {{ csrf_field() }}
                    <input id="id{{$item->id}}" name="id{{$item->id}}" class="d-none" value={{$item->id}}>
                    <h1 class="card-title pricing-card-title col-12">
                        <input type="number" id="precio{{$item->id}}"  class="form-control input-sm col-4" name="precio{{$item->id}}"  value="{{$item->precio_ref}}">
                        {{--<small class="text-muted">/ {{$item->unidad}}</small>--}}
                    </h1>
                    <button id="button{{$item->id}}" type="submit" class="btn btn-lg btn-block btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    @endforeach
    </div>
    <div class="card-deck mb-3 text-center">
    @foreach($data2 as $item)
        <div class="card mb-3 box-shadow">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">{{$item->descripcion}}</h4>
            </div>
            <div class="card-body">
                <form id="form{{$item->id}}" name="form{{$item->id}}" role="form" method="post" action="/precios/save" class="form-inline">
                    {{ csrf_field() }}
                    <input id="id{{$item->id}}" name="id{{$item->id}}" class="d-none" value={{$item->id}}>
                    <h1 class="card-title pricing-card-title col-12">
                        $ <input type="number" id="precio{{$item->id}}"  class="form-control input-sm col-4" name="precio{{$item->id}}"  value="{{$item->precio_ref}}">
                        <small class="text-muted">/ {{$item->unidad}}</small>
                    </h1>
                    <button id="button{{$item->id}}" type="submit" class="btn btn-lg btn-block btn-primary">Guardar</button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
@endsection

@section('css')
    @include('layouts.csslist')
@endsection

@section('js');
@include('layouts.jslist')
<script language="JavaScript" type="application/javascript">

    var items = <?php echo json_encode($data); ?>;
    items.forEach(function(element) {
        $('#form'+element.id).on('submit', function(e) {
            e.preventDefault();
            $('#button'+element.id).button('loading');
            var formData = $('#form'+element.id).serialize();
            save(formData);
            $('#button'+element.id).button('reset');
        });
    });

    var items2 = <?php echo json_encode($data2); ?>;
    items2.forEach(function(element) {
        $('#form'+element.id).on('submit', function(e) {
            e.preventDefault();
            $('#button'+element.id).button('loading');
            var formData = $('#form'+element.id).serialize();
            save(formData);
            $('#button'+element.id).button('reset');
        });
    });

    function save(data) {
        $.ajax('/precios/save', {
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:data,
            success: function(data) {
                iziToast.success({
                    timeout: 4000,
                    position:'topRight',
                    title: 'Listo!',
                    message: alert(JSON.stringify(data)),
                    buttons: [
                        ['<button class="text-bold"><i class="fa fa-btn fa-print"></i> Imprimir</button>', function (instance, toast, button, e, inputs) {
                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            window.location.href = '/cptes/print/rem/'+data.lastid;
                        }]
                    ]
                });
            }
        });
    }
</script>
@endsection
