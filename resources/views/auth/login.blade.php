<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="hornosg">
    <link rel="icon" href="../../favicon.ico">

    <title>CanteraNet</title>

    <!-- Scripts -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<style>
body{
    background-color: indigo!important;
}
</style>
<body>
<div class="mt-5">
    <div class="row justify-content-center">
        <div class="col-sm-4 pr-align">
                <h1 class="text-white text-center">{{ __('CanteraNet') }}</h1>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" class="container col-sm-12">
                        @csrf

                        <div class="form-group row">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        {{--<div class="form-group row">--}}
                            {{--<div class="form-check">--}}
                                {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                                {{--<label class="form-check-label" for="remember">--}}
                                    {{--{{ __('Remember Me') }}--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group row mb-0 float-right">
                            <button type="submit" class="btn btn-light font-weight-bold">
                                {{ __('Login') }}
                            </button>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>
</body>
</html>
