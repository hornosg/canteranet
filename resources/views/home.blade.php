@extends('layouts.app')

@section('content')
<div class="container">
    <div class="pt-50 visible-xs row justify-content-center ">
        <a href="/tickets/new" class="col-xs-6">
            <div class="card-link">
                <button type="button" class="btn dash-link" aria-label="Left Align">
                    <i class="material-icons md-48">note_add</i>
                    <br>
                    <span>Nuevo</span>
                </button>
            </div>
        </a>
        <a href="/tickets" class="col-xs-6">
            <div class="card-link">
                <button type="button" class="btn dash-link" aria-label="Left Align">
                    <i class="material-icons md-48">assignment</i>
                    <br>
                    <span>Resumen</span>
                </button>
            </div>
        </a>
        <a href="/titulares" class="col-xs-6">
            <div class="card-link">
                <button type="button" class="btn dash-link" aria-label="Left Align">
                    <i class="material-icons md-48">people</i>
                    <br>
                    <span>Clientes</span>
                </button>
            </div>
        </a>
    </div>
</div>
@endsection
