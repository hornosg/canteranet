@extends('layouts.app')
@section('content')
    @mobile
        <h3 class="mb-2">Listado de Clientes</h3>
        @foreach($titulares as $titular)
            <div class="list-group">
                <a href="/titulares/{{$titular->id}}" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">{{$titular->descripcion}}</h5>
                        <small>$ {{$titular->saldo}}</small>
                    </div>
                    @if (!empty($titular->direccion))
                        <p class="mb-1 float-left">Dir:{{$titular->direccion}} {{$titular->localidad}}</p>
                    @endif
                    @if (!empty($titular->telefono))
                        <p class="mb-1 float-left">Tel:{{$titular->telefono}}</p>
                    @endif
                    @if (!empty($titular->email))
                        <p class="mb-1 float-left">Mail:{{$titular->email}}</p>
                    @endif
                </a>
            </div>
        @endforeach
    @elsemobile
        <div id="toolbar">
            <h3>Listado de Clientes</h3>
        </div>
        <table
            id="table1"
            class="table table-bordered table-striped table-sm"
            data-toolbar="#toolbar"
            data-show-fullscreen="true"
            data-show-export="true"
            data-minimum-count-columns="2"
            data-pagination="true"
            data-id-field="id_titular"
            data-page-list="[10, 25, 50, 100, all]"
            data-response-handler="responseHandler">
            <thead class='thead-inverse'>
            <th data-field='id' data-align='right' class='hidden'></th>
            <th data-field='descripcion'data-align='left' data-formatter='clienteFormatter'>CLIENTE</th>
            <th data-field='cuit'       data-align='left'>CUIT</th>
            <th data-field='direccion'  data-align='left'>DIRECCION</th>
            <th data-field='localidad'  data-align='left'>LOCALIDAD</th>
            <th data-field='telefono'   data-align='left'>TELEFONO</th>
            <th data-field='email'       data-align='left'>MAIL</th>
            <th data-field='saldo'      data-align='right'>SALDO</th>
            </thead>
        </table>
    @endmobile
@endsection

@section('css')
    @include('layouts.csslist')
@endsection

@section('js');
@include('layouts.jslist')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@notmobile
    <script language='JavaScript' type='text/javascript'>
        var datos = <?php echo json_encode($titulares); ?>;
        var $table = $('#table1');
        $table.bootstrapTable('showLoading');
        $table.bootstrapTable({
            data: datos,
            exportDataType:'all',
            exportOptions:{fileName: 'clientes_proveedores'}
        });
        $table.bootstrapTable('hideLoading');

        function clienteFormatter(value, row, index) {
            return  "<a href='/titulares/"+row.id+"'>"+value+"</a>";
        }
    </script>
@endnotmobile
@endsection
