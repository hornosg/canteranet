@extends('layouts.app')

@section('content')
    <h3 id="titulo" class="text-left my-2">
        {{$titular->descripcion}}
    </h3>
    <hr class="my-3">
    <form class="form-inline d-sm-inline d-none">
        <label for="nombre">Nombre</label>
        <input type="text" class="form-control mx-sm-3 mb-2 col-sm-4" id="nombre" value={{$titular->descripcion}}>
        <label for="nombre">Cuit</label>
        <input type="text" class="form-control mx-sm-3 mb-2 col-sm-2" id="cuit" value={{$titular->cuit}}>
        <label for="nombre">Telefono</label>
        <input type="text" class="form-control mx-sm-3 mb-2 col-sm-3" id="telefono" value={{$titular->telefono}}>
        <div class="clearfix"></div>
        <label for="nombre">Direccion</label>
        <input type="text" class="form-control mx-sm-3 mb-2 col-sm-3" id="direccion" value={{$titular->direccion}}>
        <label for="nombre">Localidad</label>
        <input type="text" class="form-control mx-sm-3 mb-2 col-sm-3" id="localidad" value={{$titular->id_localidad}}>
        <label for="nombre">Mail</label>
        <input type="text" class="form-control mx-sm-3 mb-2 col-sm-2" id="mail" value={{$titular->email}}>
        <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-btn fa-save text-white"></i></button>
    </form>
    <hr class="my-3 d-sm-inline d-none">
    <h4 id="titulo" class="text-left my-2">
        @mobile
            CTA CTE
        @elsemobile
            CUENTA CORRIENTE
        @endmobile
        <small class="text-muted"> Saldo: $ {{$saldo}}</small>
    </h4>
    @mobile
        @foreach($items as $item)
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">{{$item->motivo}} <small class="text-muted"> ({{$item->cantidad}})</small></h5>
                        @if ( $item->debe > 0 )
                            <small>$ {{$item->debe}}</small>
                        @else
                            <small>$ {{$item->haber*(-1)}}</small>
                        @endif
                    </div>
                    <p class="mb-1 float-left">Nro: {{$item->numero}} <small> Fecha:{{$item->created_at}}</small></p>
                </a>
            </div>
        @endforeach
    @elsemobile
        <div id="toolbar">
            <div class="form-inline" role="form">
                <input type="number" id="preciouni"  class="form-control input-sm col-8" name="preciouni"  value="" placeholder="PRECIO UN.">
                <div class="form-group">
                    <button id="changeprice" type="button" class='btn btn-primary' onclick='ChangePrice()' data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>" title="Cambiar Precio"> <i class="fa fa-btn fa-check text-white"></i></button>
                </div>
            </div>
        </div>
        <table id="table1"
               class="table table-bordered table-striped table-sm"
               data-toolbar="#toolbar"
               data-show-fullscreen="true"
               data-show-export="true"
               data-minimum-count-columns="2"
               data-pagination="true"
               data-id-field="id"
               data-page-list="[10, 25, 50, 100, all]"
               data-response-handler="responseHandler"
               data-click-to-select="true">
            <thead class='thead-inverse'>
            <tr>
                <th data-field="state" data-checkbox="true"></th>
                <th data-field='id'    data-align='right' class='d-none'></th>
                <th data-field='created_at'    data-align='left'>FECHA</th>
                <th data-field='numero'       data-align='left'>NUMERO</th>
                <th data-field='motivo'  data-align='left'>MOTIVO</th>
                <th data-field='cantidad'  data-align='right'>CANTIDAD</th>
                <th data-field='precio'  data-align='right'>PRECIO UNI.</th>
                <th data-field='debe'  data-align='right'>DEBE</th>
                <th data-field='haber'   data-align='right'>HABER</th>
                <th data-field='saldo'      data-align='right'>SALDO</th>
            </tr>
            </thead>
        </table>
    @endmobile
@endsection

@section('css')
    @include('layouts.csslist')
@endsection

@section('js');
@include('layouts.jslist')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@notmobile
    <script language='JavaScript' type='text/javascript'>
        var idtitular = <?php echo $idtitular; ?>;
        var datos = <?php echo json_encode($items); ?>;
        var $table = $('#table1');
        $table.bootstrapTable('showLoading');
        $table.bootstrapTable({
            data: datos,
            exportDataType:'all',
            exportOptions:{fileName: 'ctacte'}
        });
        $table.bootstrapTable('hideLoading');
    </script>
@endnotmobile
<script language='JavaScript' type='text/javascript'>
    function ChangePrice(){
        $("#changeprice").button('loading');
        var preciouni = $("#preciouni").val();
        var ids = $.map($('#table1').bootstrapTable('getAllSelections'), function (row) {
            return row.id;
        });
        $.ajax('/ajaxdata/changeprice', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: { idtitular: <?php echo $idtitular; ?>, precio : preciouni, ids: ids },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                $('#table1').bootstrapTable('removeAll');
                $('#table1').bootstrapTable('showLoading');
                $('#table1').bootstrapTable('append', data.data);
                $('#titulo small').text('Saldo: $ '+data.saldo);
                $("#changeprice").button('reset');
                $("#changeprice").dequeue();
                $("#changeprice").prop("disabled", false);
                $('#table1').bootstrapTable('hideLoading');
            },
            error:function(data) {
                console.log(data);
                iziToast.warning({
                    timeout: 2000,
                    position:'center',
                    title: 'Atencion:',
                    message: data.responseJSON.message
                });
                $("#changeprice").button('reset');
                $("#changeprice").dequeue();
                $("#changeprice").prop("disabled", false);
                $('#table1').bootstrapTable('hideLoading');
            }
        });
    };
</script>
@endsection
