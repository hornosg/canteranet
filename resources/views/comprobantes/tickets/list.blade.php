@extends('layouts.app')
@section('content')
    @mobile
        <h3 class="mb-2">Resumen de Tickets</h3>
        @foreach($cptes as $cpte)
            <div class="list-group">
                <a href="/titulares/{{$cpte->id_titular}}" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">{{$cpte->titular}}</h5>
                        <small>$ {{$cpte->importe}}</small>
                    </div>
                    <p class="mb-1 float-left">PATENTE:{{$cpte->patente}}</p>
                    <small>TICKETS: {{$cpte->tickets}}</small>
                </a>
            </div>
        @endforeach
    @elsemobile
        <div id="toolbar">
            <h3>Resumen de Tickets</h3>
        </div>
        <table
            id="table1"
            class="table table-bordered table-striped table-sm"
            data-toolbar="#toolbar"
            data-show-fullscreen="true"
            data-show-export="true"
            data-minimum-count-columns="2"
            data-pagination="true"
            data-id-field="id_titular"
            data-page-list="[10, 25, 50, 100, all]"
            data-response-handler="responseHandler">
            <thead class='thead-inverse'>
            <th data-field='id_titular' data-align='right' class='d-none text-uppercase'></th>
            <th data-field='titular' data-formatter='clienteFormatter'  data-align='left'>CLIENTE</th>
            <th data-field='patente' data-align='left'  class='text-uppercase'>PATENTE</th>
            <th data-field='tickets' data-align='left'>TICKETS</th>
            <th data-field='TOSCA'   data-align='right'>TOSCA</th>
            <th data-field='DESTAPE' data-align='right'>DESTAPE</th>
            <th data-field='T.NEGRA' data-align='right'>T.NEGRA</th>
            <th data-field='FLETE'   data-align='right'>FLETE</th>
            <th data-field='importe' data-align='right'>IMPORTE</th>
            </thead>
        </table>
    @endmobile
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link " id="nav-search-tab" data-toggle="tab" href="#nav-search" role="tab" aria-controls="nav-search" aria-selected="false">
                <h5>Parametros de Busqueda</h5>
            </a>
            <a class="nav-item nav-link active" id="nav-totals-tab" data-toggle="tab" href="#nav-totals" role="tab" aria-controls="nav-totals" aria-selected="true">
                <h5>Totales</h5>
            </a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade" id="nav-search" role="tabpanel" aria-labelledby="nav-home-tab">
            <form class="form-inline pt-15">
                <div class="form-group col-sm-3">
                    <div id="reportrange" style="background: #fff;cursor: pointer;padding: 4px 0px;border: 1px solid #ccc;width: 100%;border-radius: 4px;">
                        <i class="material-icons md-18">insert_invitation</i>&nbsp;
                        <span></span> <i class="material-icons md-18">arrow_drop_down</i>
                    </div>
                </div>
                <div class="form-group col-sm-3">
                    <select id="id_cliente" name="id_cliente" type="button" class="form-control text-uppercase">
                        <option value=""></option>
                    </select>
                </div>
                <div class="form-group col-sm-4">
                    <select id="id_item" name="id_item" type="button" class="form-control text-uppercase">
                    </select>
                </div>
                <div class="form-group col-sm-2">
                    <button id="search" type="button" class='btn-search' onclick='Search()' title="Buscar">
                        <i class="material-icons md-18">search</i> Buscar
                    </button>
                </div>
            </form>
        </div>
        <div class="tab-pane fade show active" id="nav-totals" role="tabpanel" aria-labelledby="nav-profile-tab">
            <div class="row mt-40 ptb-10 totales">
                <div class="col-sm-2">
                    <div class="stat-icon">
                        <i class="material-icons md-36">speed</i>
                    </div>
                    <div class="stat">
                        <div class="value text-bold">$ {{$total}} </div>
                        <div class="name text-bold"> TOTAL </div>
                    </div>
                    <div class="progress stat-progress">
                        <div class="progress-bar" style="width: 100%;"></div>
                    </div>
                </div>
                @foreach($minerales as $mineral)
                    <div class="col-sm-2">
                        <div class="stat-icon">
                            <i class="material-icons md-36">terrain</i>
                        </div>
                        <div class="stat">
                            <div class="value text-bold">$ {{$mineral->importe}} </div>
                            <div class="name text-bold"> {{$mineral->mineral}} </div>
                        </div>
                        {{--@php--}}
                        {{--$porcentaje = $mineral->importe * 100 / $total--}}
                        {{--@endphp--}}
                        <div class="progress stat-progress">
                            {{--<div class="progress-bar" style="width: {{$porcentaje}}%;"></div>--}}
                            <div class="progress-bar" style="width: 50%;"></div>
                        </div>
                    </div>
                @endforeach

                <div class="col-sm-3">
                    <div class="stat-widget-three">
                        <div class="stat-icon">
                            <i class="material-icons md-36">swap_calls</i>
                        </div>
                        <div class="stat-content">
                            @foreach($servicios as $servicio)
                                <div class="stat-text text-bold">{{$servicio->mineral}} ${{$servicio->importe}}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    @include('layouts.csslist')
    <style>
        .select2-container {
            width: 215px !important;
        }
    </style>
@endsection

@section('js');
@include('layouts.jslist')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script language='JavaScript' type='text/javascript'>
    $("#id_cliente").select2({
        theme: 'bootstrap4',
        placeholder: "Cliente",
        allowClear: true
    });
    $("#id_item").select2({
        theme: 'bootstrap4',
        placeholder: "Producto",
        allowClear: true,
        multiple:true
    });

    $(function() {
        // var start = moment().subtract(29, 'days');
        var start = moment();
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);
    });
    function Search(){
        var startDate = $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        var endDate = $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
        var combocliente = document.getElementById("id_cliente");
        var idcliente = combocliente.options[combocliente.selectedIndex].value;
        var items = $('.leaderMultiSelctdropdown').val()
        $.ajax('/tickets/', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : { ajax:true, idtipo: 2, idcliente : idcliente, items: items, desde:startDate, hasta:endDate},
            success: function(data) {
                $('.list-group').html(data.cptes);
            }
        });
    };
    $(window).on('load', function(event){
        $.ajax('/ajaxdata/getclientes', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $.each(data, function(index) {
                    $("#id_cliente").append(
                        '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                    );
                });
                $("#id_cliente").prop("disabled", false);
            }
        });
        $("#id_item").append('<option value=""></option>');
        $.ajax('/ajaxdata/getproductos', {
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(data) {
                $.each(data, function(index) {
                    $("#id_item").append(
                        '<option value="' + data[index].id + '">' + data[index].descripcion + '</option>'
                    );
                });
                $("#id_item").prop("disabled", false);
            }
        });
    });
</script>
@notmobile
    <script language='JavaScript' type='text/javascript'>
        var datos = <?php echo json_encode($cptes); ?>;
        var $table = $('#table1');
        $table.bootstrapTable('showLoading');
        $table.bootstrapTable({
            data: datos,
            exportDataType:'all',
            exportOptions:{fileName: 'items'}
        });
        $table.bootstrapTable('hideLoading');

        function clienteFormatter(value, row, index) {
            return  "<a href='/titulares/"+row.id_titular+"'>"+value+"</a>";
        }
    </script>
@endnotmobile
@endsection
