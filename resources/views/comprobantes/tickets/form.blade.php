@php
    if ($edit==0){
        $editable='readonly';
        $visible='d-none';
        $disabled='disabled';
    }else{
        $editable=' ';
        $visible=' ';
        $disabled=' ';
    }
    if (empty($cpte)){
        $id=null;
    }else{
        $id=$cpte->id;
    }
@endphp

@extends('layouts.app')

@section('content')
        <h4 class="mt-3">Nuevo Ticket</h4>
        <form id="form1" name="form1" role="form" method="post" action="/ticket/save" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="number" id="id" name="id" class="d-none" value={{$id}} >
            <input type="number" id="id_tipocpte" name="id_tipocpte" value="2" class="d-none">
            <div class="row">
                <div class="mb-3 col-sm-4 col-xs-12">
                    <label for="numero" class="control-label">Numero de Ticket</label>
                    <input type="number" id="numero"  class="form-control input-sm text-uppercase" name="numero" {{$editable}}>
                </div>
                <div class="mb-3 col-sm-4 col-xs-12">
                    <label for="id_titular" class="control-label">Cliente</label>
                    <select id="id_titular" name="id_titular" class="form-control text-uppercase" {{$disabled}}>
                        <option value=""></option>
                        @if (!empty($cpte))
                            @foreach(DB::table('titulares')->where('id_tipotitular', 4)->where('activo', 1)->get() as $tipotitular)
                                @if ( $cpte->id_titular == $tipotitular->id )
                                    <option value="{{ $tipotitular->id }}" selected>{{ $tipotitular->descripcion }}</option>
                                @else
                                    <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                @endif
                            @endforeach
                        @else
                            @foreach(DB::table('titulares')->where('id_tipotitular', 4)->where('activo', 1)->get() as $tipotitular)
                                <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="mb-3 col-sm-4 col-xs-12">
                    <label for="patente" class="control-label">Patente</label>
                    <input id="patente"  class="form-control input-sm text-uppercase" name="patente"  value="" {{$editable}}>
                </div>
            </div>
            <div class="row">
                <div class="mb-3 col-sm-5 col-xs-12">
                    <label for="id_mineral" class="control-label col-sm-12">Mineral</label>
                    @foreach(DB::table('items')->where('activo', 1)->where('id_tipoitem', 1)->get() as $item)
                        @if (!empty($cpte))
                            @if ( $cpte->id_item == $item->id )
                                <div class="form-check-inline col-sm-2">
                                    <input class="form-check-input" type="radio" name="id_mineral" id="item{{$item->id}}" value="{{$item->id}}" checked>
                                    <label class="form-check-label" for="item{{$item->id}}">{{$item->descripcion}}</label>
                                </div>
                            @else
                                <div class="form-check-inline col-sm-2">
                                    <input class="form-check-input" type="radio" name="id_mineral" id="item{{$item->id}}" value="{{$item->id}}">
                                    <label class="form-check-label" for="item{{$item->id}}">{{$item->descripcion}}</label>
                                </div>
                            @endif
                        @else
                            @if($item->id==1)
                                @php $checked='checked'; @endphp
                            @endif
                            <div class="form-check-inline col-sm-2">
                                <input class="form-check-input" type="radio" name="id_mineral" id="item{{$item->id}}" value="{{$item->id}}" $checked>
                                <label class="form-check-label" for="item{{$item->id}}">{{$item->descripcion}}</label>
                            </div>
                        @endif
                    @endforeach
                </div>
                <div class="mb-3 col-sm-2 col-xs-6">
                    <label for="cantidad" class="control-label">Cantidad</label>
                    <input type="number" id="cantidad"  class="form-control input-sm text-uppercase" name="cantidad"  value="1" {{$editable}}>
                </div>
                <div class="mb-3 col-sm-2 col-xs-6">
                    <label for="tipo" class="control-label col-sm-12">Tipo de Camion</label>
                    <div class="form-check-inline col-sm-3">
                        <input class="form-check-input" type="checkbox" name="tipo" id="tipo" value="" checked>
                        <label class="form-check-label">CHASIS</label>
                    </div>
                </div>
                <div class="mb-3 col-sm-3 col-xs-12">
                    <label for="servicio" class="control-label col-sm-12">Servicios</label>
                    @foreach(DB::table('items')->where('activo', 1)->where('id_tipoitem', 2)->orderBy('id', 'desc')->get() as $item)
                        @if (!empty($cpte))
                            @if ( $cpte->id_item == $item->id )
                                <div class="form-check-inline col-sm-2">
                                    <input class="form-check-input" type="checkbox" name="servicio[]" id="item{{$item->id}}" value="{{$item->id}}" checked>
                                    <label class="form-check-label" for="item{{$item->id}}">{{$item->descripcion}}</label>
                                </div>
                            @else
                                <div class="form-check-inline col-sm-2">
                                    <input class="form-check-input" type="checkbox" name="servicio[]" id="item{{$item->id}}" value="{{$item->id}}">
                                    <label class="form-check-label" for="item{{$item->id}}">{{$item->descripcion}}</label>
                                </div>
                            @endif
                        @else
                            @if($item->id==1)
                                @php $checked='checked'; @endphp
                            @endif
                            <div class="form-check-inline col-sm-2 col-xs-12">
                                <input class="form-check-input" type="checkbox" name="servicios[]" id="item{{$item->id}}" value="{{$item->id}}" $checked>
                                <label class="form-check-label" for="item{{$item->id}}">{{$item->descripcion}}</label>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div id="divflete" class="mb-3 col-xs-12">
                <div class="row">
                    <div class="mb-3 col-xs-12 col-sm-3">
                        <label for="id_flete" class="control-label">Realiza el Flete</label>
                        <select id="id_flete" name="id_flete" class="form-control text-uppercase" {{$disabled}}>
                            <option value=""></option>
                            @if (!empty($cpte))
                                @foreach(DB::table('titulares')->where('id_tipotitular', 4)->where('activo', 1)->get() as $tipotitular)
                                    @if ( $cpte->id_titular == $tipotitular->id )
                                        <option value="{{ $tipotitular->id }}" selected>{{ $tipotitular->descripcion }}</option>
                                    @else
                                        <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                    @endif
                                @endforeach
                            @else
                                @foreach(DB::table('titulares')->where('id_tipotitular', 4)->where('activo', 1)->get() as $tipotitular)
                                    <option value="{{$tipotitular->id}}">{{$tipotitular->descripcion}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="mb-3 col-xs-6 col-sm-3">
                        <label for="kms" class="control-label">Kms</label>
                        <input type="number" id="kms"  class="form-control input-sm text-uppercase" name="kms"  value="" {{$editable}}>
                    </div>
                    <div class="mb-3 col-xs-6 col-sm-3">
                        <label for="gasoil" class="control-label">GasOil</label>
                        <input type="number" id="gasoil"  class="form-control input-sm text-uppercase" name="gasoil"  value="" {{$editable}}>
                    </div>
                    <div class="mb-3 col-xs-6 col-sm-3">
                        <label for="adelanto" class="control-label">Adelanto</label>
                        <input type="number" id="adelanto"  class="form-control input-sm text-uppercase" name="adelanto"  value="" {{$editable}}>
                    </div>
                </div>
            </div>
            <div class="mb-3 col-sm-2 col-xs-6">
                <label for="tipo" class="control-label col-sm-12 text-center">Medio de Pago</label>
                <div class="form-check text-left">
                    <input class="form-check-input" type="radio" name="id_mediopago" id="medio1" value=1" checked>
                    <label class="form-check-label" for="exampleRadios1">
                        Cuenta Corriente
                    </label>
                </div>
                <div class="form-check text-left">
                    <input class="form-check-input" type="radio" name="id_mediopago" id="medio2" value=2>
                    <label class="form-check-label" for="exampleRadios2">
                        Efectivo
                    </label>
                </div>
            </div>            
            <div class="buttons-bar">
                <button id="btnsubmit" type="submit" class="btn btn-primary pull-right col-sm-2 col-xs-5"
                        data-loading-text="Guardando...">Guardar</button>
                <a herf="/home" class="btn btn-secondary pull-right col-sm-2 col-xs-5" >Salir</a>
            </div>
        </form>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <link href="https://raw.githack.com/ttskch/select2-bootstrap4-theme/master/dist/select2-bootstrap4.min.css" rel="stylesheet">
@endsection

@section('js');
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script>
    $("#id_titular").select2({
        theme: 'bootstrap4',
        tags: true,
        placeholder:'Ingrese el Cliente',
        allowClear: true
    });
    $("#id_flete").select2({
        theme: 'bootstrap4',
        placeholder:'Ingrese el Flete',
        allowClear: true
    });
    $('#form1').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: true, // do not focus the last invalid input
        ignore: "",
        rules: {
            id_titular: "required",
            id_mineral: "required",
            cantidad: "required"
        },
        messages: {
            id_titular: "Por favor, Ingrese el Cliente",
            id_mineral: "Por favor, Ingrese el Mineral",
            cantidad: "Por favor, Ingrese la cantidd"
        },
        invalidHandler: function (event, validator) { //display error alert on form submit
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },
        errorPlacement: function (error, element) { // render error placement for each input type
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) { // hightlight error inputs
            $(element).closest('.mb-3').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.mb-3').removeClass('has-error'); // set error class to the control group
        },
        success: function (label, element) {
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.mb-3').removeClass('has-error').addClass('has-success'); // set success class to the control group
            icon.removeClass("fa-warning").addClass("fa-check");
        }
    });

    $('#form1').submit(function (event) {
        var formData = $('#form1').serialize();
        $.ajax('/ticket/save', {
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data:formData,
            success: function(data) {
                iziToast.success({
                    timeout: 4000,
                    position:'topRight',
                    title: 'Listo!',
                    message: data.msg
                });
                window.location.replace("http://canteranet.com.ar/home");
            }
        });
        event.preventDefault();
    });
    $(function () {
        $('#item4').click(function () {
            if($('#item4').prop('checked')==true){
                $('#divflete').removeClass('d-none');
            }else{
                $('#divflete').addClass('d-none');
            }
        });
    });
    window.onload = function() {
        $('#divflete').addClass('d-none');
    };
</script>
@endsection
