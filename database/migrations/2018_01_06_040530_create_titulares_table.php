<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitularesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('titulares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->unsignedInteger('id_tipotitular');
            $table->unsignedInteger('id_listaprecio')->nullable();
            $table->string('imagen')->nullable();
            $table->string('cuit')->nullable();
            $table->unsignedInteger('id_tipoiva')->nullable();
            $table->string('direccion')->nullable();
            $table->unsignedInteger('id_localidad')->nullable();
            $table->string('telefono')->nullable();
            $table->string('email')->nullable();
            $table->string('web')->nullable();
            $table->string('anotaciones')->nullable();
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->foreign('id_tipotitular')->references('id')->on('sys_tipotitulares');
            $table->foreign('id_tipoiva')->references('id')->on('sys_tipoiva');
            $table->foreign('id_localidad')->references('id')->on('conf_localidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('titulares');
    }
}
