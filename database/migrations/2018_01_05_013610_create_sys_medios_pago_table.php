<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysMediosPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_medios_pago', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->boolean('banco')->default(0);
            $table->boolean('cuotas')->default(0);
            $table->boolean('numero')->default(0);
            $table->boolean('fecha')->default(0);
            $table->boolean('fecha_cobro')->default(0);
            $table->boolean('activo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_medios_pago');
    }
}
