<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesMpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes_mp', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_cpte');
            $table->unsignedInteger('id_mediopago');
            $table->unsignedInteger('id_banco')->nullable();
            $table->integer('cuotas')->nullable();
            $table->string('numero')->nullable();
            $table->date('fecha')->nullable();
            $table->date('fecha_cobro')->nullable();
            $table->decimal('importe', 10, 2)->nullable();
            $table->timestamps();
            $table->unsignedInteger('created_us')->nullable();
            $table->unsignedInteger('updated_us')->nullable();

            $table->foreign('id_cpte')->references('id')->on('comprobantes');
            $table->foreign('id_mediopago')->references('id')->on('sys_medios_pago');
            $table->foreign('id_banco')->references('id')->on('conf_bancos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes_mp');
    }
}
