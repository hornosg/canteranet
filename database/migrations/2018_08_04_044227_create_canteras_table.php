<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCanterasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canteras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->string('alias')->nullable();
            $table->string('direccion')->nullable();
            $table->unsignedInteger('id_localidad');
            $table->string('telefono')->nullable();
            $table->string('coordenadas')->nullable();
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->foreign('id_localidad')->references('id')->on('conf_localidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sucursales');
    }
}
