<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->unsignedInteger('id_tipoitem');
            $table->decimal('precio_ref', 10, 2)->nullable();
            $table->unsignedInteger('id_unidad')->nullable();
            $table->string('observaciones')->nullable();
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->foreign('id_tipoitem')->references('id')->on('sys_tipoitems');
            $table->foreign('id_unidad')->references('id')->on('conf_unidades_medida');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
