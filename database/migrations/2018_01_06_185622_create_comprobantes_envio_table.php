<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesEnvioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes_envio', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_cpte');
            $table->dateTime('fecha')->nullable();
            $table->string('direccion')->nullable();
            $table->unsignedInteger('id_localidad')->nullable();
            $table->decimal('importe', 10, 2)->nullable();
            $table->string('observaciones')->nullable();
            $table->timestamps();
            $table->unsignedInteger('created_us')->nullable();
            $table->unsignedInteger('updated_us')->nullable();

            $table->foreign('id_cpte')->references('id')->on('comprobantes');
            $table->foreign('id_localidad')->references('id')->on('conf_localidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes_envio');
    }
}
