<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_cpte');
            $table->unsignedInteger('id_item');
            $table->decimal('preciouni', 10, 2)->nullable();
            $table->decimal('cantidad', 6, 2)->nullable();
            $table->decimal('importe', 10, 2)->nullable();
            $table->timestamps();
            $table->unsignedInteger('created_us')->nullable();
            $table->unsignedInteger('updated_us')->nullable();
            $table->foreign('id_cpte')->references('id')->on('comprobantes');
            $table->foreign('id_item')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes_items');
    }
}
