<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprobantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_tipocpte');
            $table->char('serie')->nullable();
            $table->integer('numero')->nullable();
            $table->unsignedInteger('id_titular');
            $table->decimal('total', 10, 2)->nullable();
            $table->string('observaciones')->nullable();
            $table->unsignedInteger('id_cpterel')->nullable();
            $table->unsignedInteger('id_estado')->nullable();
            $table->timestamps();
            $table->unsignedInteger('created_us')->nullable();
            $table->unsignedInteger('updated_us')->nullable();
            $table->softDeletes();
            $table->foreign('id_tipocpte')->references('id')->on('sys_tipocomprobantes');
            $table->foreign('id_titular')->references('id')->on('titulares');
            $table->foreign('id_estado')->references('id')->on('sys_estados');
        });
        Schema::table('comprobantes', function (Blueprint $table) {
            $table->foreign('id_cpterel')->references('id')->on('comprobantes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes');
    }
}
