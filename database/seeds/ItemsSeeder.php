<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Item;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'TOSCA','id_tipoitem'=>1,'precio_ref'=>300,'id_unidad'=>1,'activo'=>true ],
            ['descripcion'=>'DESTAPE','id_tipoitem'=>1,'precio_ref'=>350,'id_unidad'=>1,'activo'=>true ],
            ['descripcion'=>'T.NEGRA','id_tipoitem'=>1,'precio_ref'=>400,'id_unidad'=>1,'activo'=>true ],
            ['descripcion'=>'FLETE','id_tipoitem'=>2,'precio_ref'=>200,'id_unidad'=>3,'activo'=>true ],
            ['descripcion'=>'GUIA','id_tipoitem'=>2,'precio_ref'=>250,'id_unidad'=>5,'activo'=>true ],
            ['descripcion'=>'GASOIL','id_tipoitem'=>3,'precio_ref'=>1,'id_unidad'=>5,'activo'=>true ],
            ['descripcion'=>'ADELANTO','id_tipoitem'=>3,'precio_ref'=>1,'id_unidad'=>5,'activo'=>true ]
        ];

        foreach($items as $item){
            Item::create($item);
        }
    }
}
