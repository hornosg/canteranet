<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\TipoComprobante;

class TipoComprobanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('sys_tipocomprobantes')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'PRESUPUESTO','activo'=>true],
            ['descripcion'=>'TICKET','activo'=>true ],
            ['descripcion'=>'FACTURA','activo'=>true ],
            ['descripcion'=>'RECIBO','activo'=>true ],
            ['descripcion'=>'PAGO','activo'=>true],
            ['descripcion'=>'COBRANZA','activo'=>true],
            ['descripcion'=>'NOTA DE CREDITO','activo'=>true],
            ['descripcion'=>'NOTA DE DEBITO','activo'=>true]
        ];

        foreach($items as $item){
            TipoComprobante::create($item);
        }
    }
}
