<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\MedioPago;

class MediosPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'EFECTIVO',     'cuotas'=>false,'numero'=>false,'fecha'=>false, 'fecha_cobro'=>false,   'banco'=>false, 'activo'=>true ],
            ['descripcion'=>'CTA CTE',      'cuotas'=>false,'numero'=>false,'fecha'=>false, 'fecha_cobro'=>false,   'banco'=>false, 'activo'=>true ],
            ['descripcion'=>'T.DEBITO',     'cuotas'=>false,'numero'=>true, 'fecha'=>false, 'fecha_cobro'=>false,   'banco'=>false, 'activo'=>true ],
            ['descripcion'=>'T.CREDITO',    'cuotas'=>true, 'numero'=>true, 'fecha'=>false, 'fecha_cobro'=>false,   'banco'=>false, 'activo'=>true ],
            ['descripcion'=>'CHEQUE',       'cuotas'=>false,'numero'=>true, 'fecha'=>true,  'fecha_cobro'=>true,    'banco'=>false, 'activo'=>true ],
            ['descripcion'=>'DEPOSITO',     'cuotas'=>false,'numero'=>true, 'fecha'=>false, 'fecha_cobro'=>false,   'banco'=>false, 'activo'=>true ],
            ['descripcion'=>'TRANSFERENCIA','cuotas'=>false,'numero'=>true, 'fecha'=>false, 'fecha_cobro'=>false,   'banco'=>false, 'activo'=>true ]
        ];

        foreach($items as $item){
            MedioPago::create($item);
        }
    }
}
