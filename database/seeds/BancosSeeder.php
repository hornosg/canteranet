<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Banco;

class BancosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'NACION','activo'=>true],
            ['descripcion'=>'PROVINCIA BS AS','activo'=>true],
            ['descripcion'=>'HIPOTECARIO','activo'=>true],
            ['descripcion'=>'CIUDAD','activo'=>true],
            ['descripcion'=>'SANTANDER RIO','activo'=>true],
            ['descripcion'=>'GALICIA','activo'=>true],
            ['descripcion'=>'PATAGONIA','activo'=>true],
            ['descripcion'=>'ICBC','activo'=>true],
            ['descripcion'=>'ITAU','activo'=>true],
            ['descripcion'=>'HSBC','activo'=>true],
            ['descripcion'=>'CREDICOOP','activo'=>true],
            ['descripcion'=>'MACRO','activo'=>true],
            ['descripcion'=>'COMAFI','activo'=>true],
            ['descripcion'=>'FRANCES','activo'=>true],
            ['descripcion'=>'ENTRE RIOS','activo'=>true],
            ['descripcion'=>'CHACO','activo'=>true],
            ['descripcion'=>'CIUDAD','activo'=>true]
        ];

        foreach($items as $item){
            Banco::create($item);
        }
    }
}
