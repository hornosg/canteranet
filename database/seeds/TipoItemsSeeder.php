<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\TipoItem;

class TipoItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'MINERALES','activo'=>true ],
            ['descripcion'=>'SERVICIOS','activo'=>true ],
            ['descripcion'=>'DEBITOS','activo'=>true ]
        ];

        foreach($items as $item){
            TipoItem::create($item);
        }
    }
}
