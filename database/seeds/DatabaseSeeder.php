<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([UsersTableSeeder::class,

                     TipoEstadosSeeder::class,
                     TipoComprobanteSeeder::class,
                     TipoItemsSeeder::class,
                     TipoIvaSeeder::class,
                     TipoTitularSeeder::class,

                     BancosSeeder::class,
                     EstadosSeeder::class,
                     PaisesSeeder::class,
                     ProvinciasSeeder::class,
                     LocalidadesSeeder::class,
                     CanterasSeeder::class,
                     MediosPagoSeeder::class,
                     UnidadesSeeder::class,

                     TitularSeeder::class,
                     ItemsSeeder::class
        ]);
    }
}
