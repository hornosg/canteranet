<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\TipoTitular;

class TipoTitularSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'EMPRESA','activo'=>true ],
            ['descripcion'=>'CLIENTES','activo'=>true ],
            ['descripcion'=>'PROVEEDORES','activo'=>true ],
            ['descripcion'=>'CLIENTE/PROVEEDOR','activo'=>true ]
        ];

        foreach($items as $item){
            TipoTitular::create($item);
        }
    }
}
