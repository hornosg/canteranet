<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\TipoEstado;

class TipoEstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'DE COMPROBANTES','activo'=>true ]
        ];

        foreach($items as $item){
            TipoEstado::create($item);
        }
    }
}
