<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Estado;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('sys_estados')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'PAGO','id_tipoestado'=>1,'activo'=>true ],
            ['descripcion'=>'PENDIENTE','id_tipoestado'=>1,'activo'=>true ],
            ['descripcion'=>'REGISTRADO','id_tipoestado'=>1,'activo'=>true ],
            ['descripcion'=>'ACREDITADO','id_tipoestado'=>1,'activo'=>true ]
        ];

        foreach($items as $item){
            Estado::create($item);
        }
    }
}
