<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\UnidadMedida;

class UnidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'TN','activo'=>true],
            ['descripcion'=>'MT3','activo'=>true],
            ['descripcion'=>'KMS','activo'=>true],
            ['descripcion'=>'HS','activo'=>true],
            ['descripcion'=>'UNI','activo'=>true]
        ];

        foreach($items as $item){
            UnidadMedida::create($item);
        }
    }
}
