<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Titular;

class TitularSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::select('SET FOREIGN_KEY_CHECKS=0');
        DB::table('titulares')->truncate();
        Model::unguard();
        $items = [
            ['descripcion'=>'EMPRESA','id_tipotitular'=>1,'cuit'=>'CUIT','direccion'=>'CALLE NRO','id_localidad'=>1,'telefono'=>'0110800666'],
            ['descripcion'=>'CONSUMIDOR FINAL','id_tipotitular'=>2,'id_tipoiva'=>1,'cuit'=>null,]
        ];

        foreach($items as $item){
            Titular::create($item);
        }
    }
}
