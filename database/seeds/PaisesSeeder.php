<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pais;

class PaisesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'ARGENTINA','activo'=>true],
            ['descripcion'=>'BRASIL','activo'=>true],
            ['descripcion'=>'CHILE','activo'=>true],
            ['descripcion'=>'URUGUAY','activo'=>true],
            ['descripcion'=>'PARAGUAY','activo'=>true],
            ['descripcion'=>'BOLIVIA','activo'=>true],
            ['descripcion'=>'PERU','activo'=>true]
        ];

        foreach($items as $item){
            Pais::create($item);
        }
    }
}
