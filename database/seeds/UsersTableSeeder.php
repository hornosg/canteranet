<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'hornosg@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('01129981..'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'name' => 'Admin Test',
            'email' => 'admin@test.com',
            'email_verified_at' => now(),
            'password' => Hash::make('011..123'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
