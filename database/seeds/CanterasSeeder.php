<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cantera;

class CanterasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $items = [
            ['descripcion'=>'CANTERA 1','alias'=>'C1','activo'=>true,'direccion'=>'...','id_localidad'=>4],
            ['descripcion'=>'CANTERA 2','alias'=>'C2','activo'=>true,'direccion'=>'...','id_localidad'=>12]
        ];

        foreach($items as $item){
            Cantera::create($item);
        }

    }
}
