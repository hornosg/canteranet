<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('login');
});
Auth::routes();
Route::middleware('auth')->group(function () {
    Route::view('/formtest', 'formtest');
    Route::get('/home', 'HomeController@index');
    Route::get('/', 'HomeController@index');
    Route::get('tickets/new', 'ComprobantesController@new');
    Route::post('ticket/save','ComprobantesController@save');
    Route::get('tickets/', ['as' => 'tickets.list', 'uses' => 'ComprobantesController@listCptes']);

    Route::get('titulares/', ['as' => 'titulares.list', 'uses' => 'TitularesController@listTitulares']);
    Route::get('titulares/{id}', ['as' => 'titulares.ctacte', 'uses' => 'TitularesController@ctacte']);

    Route::get('ajaxdata/getproductos', 'AjaxDataController@getproductos');
    Route::get('ajaxdata/getclientes', 'AjaxDataController@getclientes');
    Route::get('ajaxdata/changeprice', 'ComprobantesController@changePrice');

    Route::get('precios/', ['as' => 'items.list', 'uses' => 'ItemsController@listItems']);
    Route::post('precios/save', 'ItemsController@save');
});
